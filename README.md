# Brambox
Unified tools for generating PR curves, converting image data annotation sets and more

## How to install

```bash
pip install .
# OR
python setup.py install
```
NOTE: This project is python 3.6 and higher so on some systems you might want to use 'python3.6' instead of 'python'

## Usage
The toolbox contains both library packages and scripts.

If you installed brambox you can just run brambox scripts from anywhere on the commandline.
For more about there usage:
```
some_brambox_script.py --help
```

If you installed brambox you can also import brambox packages in your own python program with:
```
import brambox
```

## Contribution guidelines

See [the contribution guidelines](CONTRIBUTING.md)
