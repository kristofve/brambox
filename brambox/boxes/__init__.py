#
# Bounding box parsers
# Copyright EAVISE
#

from . import annotations
from . import detections
from .util import *
from .formats import *
